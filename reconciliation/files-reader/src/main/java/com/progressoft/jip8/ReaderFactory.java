package com.progressoft.jip8;


import java.util.Map;
import java.util.Objects;

public class ReaderFactory {
    public static Reader<Map<String, BankTransaction>> getReader(String extension) {
        throwIfNullExtension(extension);
        if (isCsv(extension))
            return new CSVReader();
        if (isJson(extension))
            return new JSONReader();
        throw new IllegalArgumentException("Invalid extension");
    }

    private static void throwIfNullExtension(String extension) {
        if (Objects.isNull(extension)) {
            throw new NullPointerException("Null extension");
        }
    }

    private static boolean isCsv(String extension) {
        return extension.equalsIgnoreCase("csv");
    }

    private static boolean isJson(String extension) {
        return extension.equalsIgnoreCase("json");
    }
}
