package com.progressoft.jip8;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.function.Function;


public class FunctionBasedReader<T> {
    private Function<String, T> function;

    public FunctionBasedReader(Function<String, T> function) {
        this.function = function;
    }

    public ArrayList<T> read(Path fileName) throws IOException {
        String line;
        ArrayList<T> transactionList = new ArrayList<>();
        try (BufferedReader reader = Files.newBufferedReader(fileName)) {
            skipHeader(reader);
            while ((line = reader.readLine()) != null) {
                transactionList.add(function.apply(line));
            }
            return transactionList;
        }
    }

    public void skipHeader(BufferedReader reader) throws IOException {
        reader.readLine();
    }
}
