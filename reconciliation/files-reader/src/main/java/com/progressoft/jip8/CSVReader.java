package com.progressoft.jip8;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CSVReader extends Reader<Map<String, BankTransaction>> {
    @Override
    public Map<String, BankTransaction> read(Path fileName) throws IOException {
        String line;
        Map<String, BankTransaction> transactionMap = new HashMap<>();
        try (BufferedReader reader = Files.newBufferedReader(fileName)) {
            skipHeader(reader);
            while ((line = reader.readLine()) != null) {
                BankTransaction transaction = createTransaction(line);
                transactionMap.put(transaction.getReference(), transaction);
            }
            return transactionMap;
        }
    }

    private BankTransaction createTransaction(String line) {
        String[] csvTransaction = line.split(",");

        String reference = csvTransaction[0];
        String currencyCode = csvTransaction[3];
        BigDecimal amount = BigDecimal.valueOf(Double.parseDouble(csvTransaction[2]));
        Date date = convertToDate(csvTransaction[5]);

        return new BankTransaction(reference, currencyCode, amount, date);
    }


}
