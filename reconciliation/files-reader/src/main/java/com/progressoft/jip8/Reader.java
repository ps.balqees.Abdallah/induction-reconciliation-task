package com.progressoft.jip8;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

// TODO you should use generic .. Done
public abstract class Reader<TYPE> {

    public abstract TYPE read(Path fileName) throws IOException, ParseException;

    public void skipHeader(BufferedReader reader) throws IOException {
        reader.readLine();
    }

    public Date convertToDate(String strDate) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(strDate);
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
