package com.progressoft.jip8;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.progressoft.jip8.BankTransaction;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JSONReader extends Reader<Map<String, BankTransaction>> {
    @Override
    public Map<String, BankTransaction> read(Path fileName) throws IOException {

        try (BufferedReader bufferedReader = Files.newBufferedReader(fileName)) {
            String line;
            String fileInput = "";
            while ((line = bufferedReader.readLine()) != null) {
                fileInput += line + "\n";
            }
            return parsingJSON(fileInput);
        }
    }

    private Map<String, BankTransaction> parsingJSON(String fileInput) {
        ArrayList<BankTransaction> bankTransList;
        Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
        bankTransList = gson.fromJson(fileInput
                , new TypeToken<List<BankTransaction>>() {
                }.getType());

        return convertArrayListToMap(bankTransList);
    }

    private Map<String, BankTransaction> convertArrayListToMap(ArrayList<BankTransaction> bankTransactions) {
        HashMap<String, BankTransaction> transactionHashMap = new HashMap<>();

        for (BankTransaction bankTransaction : bankTransactions) {
            transactionHashMap.put(bankTransaction.getReference(), bankTransaction);
        }
        return transactionHashMap;
    }

}
