//package com.progressoft.jip8;
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.math.BigDecimal;
//import java.nio.file.Files;
//import java.nio.file.Path;
//import java.util.*;
//import java.util.function.Function;
//
//
////removed
//public class MatchReader extends Reader<ArrayList<BankTransaction>> {
//    // TODO duplicate code in readers .. Done
//    // TODO you can have one implementation accpeting a Function<String,T> that converts a line to T ..Done
//
//    @Override
//    public ArrayList<BankTransaction> read(Path fileName) throws IOException {
//        Function<String, BankTransaction> function = (String str) -> {
//           return createTransaction(str);
//        };
//
//        return doRead(fileName, function);
//    }
//
//    private ArrayList<BankTransaction> doRead(Path fileName, Function<String, BankTransaction> function) throws IOException {
//        String line;
//        ArrayList<BankTransaction> transactionList = new ArrayList<>();
//        try (BufferedReader reader = Files.newBufferedReader(fileName)) {
//            skipHeader(reader);
//            while ((line = reader.readLine()) != null) {
//                transactionList.add(function.apply(line));
//            }
//            return transactionList;
//        }
//    }
//
//    private BankTransaction createTransaction(String line) {
//        String[] transaction = line.split(",");
//
//        String reference = transaction[0];
//        String currencyCode = transaction[2];
//        BigDecimal amount = BigDecimal.valueOf(Double.parseDouble(transaction[1]));
//        Date date = convertToDate(transaction[3]);
//
//        return new BankTransaction(reference, currencyCode, amount, date);
//    }
//}
