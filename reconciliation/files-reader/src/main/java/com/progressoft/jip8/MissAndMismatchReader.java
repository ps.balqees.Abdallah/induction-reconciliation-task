//package com.progressoft.jip8;
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.math.BigDecimal;
//import java.nio.file.Files;
//import java.nio.file.Path;
//import java.util.*;
//
////removed
//public class MissAndMismatchReader extends Reader<ArrayList<TransactionTypePair>> {
//
//    @Override
//    public ArrayList<TransactionTypePair> read(Path fileName) throws IOException {
//        String line;
//        ArrayList<TransactionTypePair> transactionList = new ArrayList<>();
//        try (BufferedReader reader = Files.newBufferedReader(fileName)) {
//            skipHeader(reader);
//            while ((line = reader.readLine()) != null) {
//                transactionList.add(createTransaction(line));
//            }
//            return transactionList;
//        }
//    }
//
//    private TransactionTypePair createTransaction(String line) {
//        String[] transaction = line.split(",");
//
//        String type = transaction[0];
//        String reference = transaction[1];
//        String currencyCode = transaction[3];
//        BigDecimal amount = BigDecimal.valueOf(Double.parseDouble(transaction[2]));
//        Date date = convertToDate(transaction[4]);
//
//        BankTransaction bankTransaction = new BankTransaction(reference, currencyCode, amount, date);
//        return new TransactionTypePair(type, bankTransaction);
//    }
//}
