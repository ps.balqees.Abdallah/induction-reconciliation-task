package com.progressoft.jip8;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.function.Function;

public class MissAndMismatchReaderTest {

    @Test
    public void givenNullReference_whenReadMissAndMisMatch_thenFail() throws IOException {

        Path resultFile = Paths.get(ClassLoader.getSystemResource("nullRef-miss-mismatch.csv").getPath());
        Function<String, TransactionWithType> function = getMissAndMismatchFunction();
        FunctionBasedReader<TransactionWithType> reader = new FunctionBasedReader<>(function);
        ArrayList<TransactionWithType> transaction = reader.read(resultFile);

        Assertions.assertEquals("", transaction.get(0).getBankTransaction().getReference());
    }

    @Test
    public void givenNullCurrencyCode_whenReadMissAndMisMatch_thenFail() throws IOException{

        Path result = Paths.get(ClassLoader.getSystemResource("nullCurrency-miss-mismatch.csv").getPath());
        Function<String, TransactionWithType> function = getMissAndMismatchFunction();
        FunctionBasedReader<TransactionWithType> reader = new FunctionBasedReader<>(function);
        ArrayList<TransactionWithType> transaction = reader.read(result);
        Assertions.assertEquals("", transaction.get(0).getBankTransaction().getCurrencyCode());

    }

    @Test
    public void givenValidMismatchFileBetweenCSVAndJson_whenReadMissAndMismatch_thenPass() throws IOException, ParseException {

        Path resultFile = Paths.get(ClassLoader.getSystemResource("miss-file.csv").getPath());
        Function<String, TransactionWithType> function = getMissAndMismatchFunction();
        FunctionBasedReader<TransactionWithType> reader = new FunctionBasedReader<>(function);
        ArrayList<TransactionWithType> transaction = reader.read(resultFile);


        Assertions.assertEquals("TR-47884222204", transaction.get(0).getBankTransaction().getReference());
        Assertions.assertEquals("TR-47884222217", transaction.get(1).getBankTransaction().getReference());
        Assertions.assertEquals("TR-47884222245", transaction.get(2).getBankTransaction().getReference());
    }

    @Test
    public void givenValidMissFileBetweenCSVAndJson_whenReadMissAndMismatch_thenPass() throws IOException, ParseException {

        Path resultFile = Paths.get(ClassLoader.getSystemResource("mismatch-file.csv").getPath());
        Function<String, TransactionWithType> function = getMissAndMismatchFunction();
        FunctionBasedReader<TransactionWithType> reader = new FunctionBasedReader<>(function);
        ArrayList<TransactionWithType> transaction = reader.read(resultFile);

        Assertions.assertEquals("TR-47884222202", transaction.get(0).getBankTransaction().getReference());
        Assertions.assertEquals("TR-47884222202", transaction.get(1).getBankTransaction().getReference());
        Assertions.assertEquals("TR-47884222205", transaction.get(2).getBankTransaction().getReference());
        Assertions.assertEquals("TR-47884222205", transaction.get(3).getBankTransaction().getReference());
    }

    private Function<String, TransactionWithType> getMissAndMismatchFunction() {
        return (String line) -> {
            String[] transaction = line.split(",");

            String type = transaction[0];
            String reference = transaction[1];
            String currencyCode = transaction[3];
            BigDecimal amount = BigDecimal.valueOf(Double.parseDouble(transaction[2]));
            Date date = convertToDate(transaction[4]);
            BankTransaction bankTransaction = new BankTransaction(reference, currencyCode, amount, date);
            return new TransactionWithType(type, bankTransaction);
        };
    }

    private Date convertToDate(String strDate) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(strDate);
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
