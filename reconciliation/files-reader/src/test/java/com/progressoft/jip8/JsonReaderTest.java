package com.progressoft.jip8;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.Map;


public class JsonReaderTest {
    @Test
    public void givenNullReference_whenGetReferenceInJSON_thenFail() throws IOException, ParseException {

        Path jsonFile = Paths.get(ClassLoader.getSystemResource("nullReference-file.json").getPath());
        Reader<Map<String, BankTransaction>> reader = new JSONReader();
        Map<String, BankTransaction> jsonFileTransaction  = reader.read(jsonFile);

        for (Map.Entry<String, BankTransaction> entry : jsonFileTransaction.entrySet())
            Assertions.assertEquals("", entry.getValue().getReference());
    }

    @Test
    public void givenNullCurrency_whenGetCurrencyCodeInJson_thenFail() throws IOException, ParseException {
        Path jsonFile = Paths.get(ClassLoader.getSystemResource("nullCurrencyCode-file.json").getPath());
        Reader<Map<String, BankTransaction>> reader = new JSONReader();
        Map<String, BankTransaction> jsonFileTransaction = reader.read(jsonFile);

        for (Map.Entry<String, BankTransaction> entry : jsonFileTransaction.entrySet())
            Assertions.assertEquals("", entry.getValue().getCurrencyCode());
    }

}
