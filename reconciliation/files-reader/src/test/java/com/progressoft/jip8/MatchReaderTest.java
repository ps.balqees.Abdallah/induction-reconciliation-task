package com.progressoft.jip8;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.function.Function;

public class MatchReaderTest {
    @Test
    public void givenNullReference_whenReadMatchFile_thenFail() throws IOException {

        Path matchFile = Paths.get(ClassLoader.getSystemResource("nullRef-match-file.csv").getPath());
        Function<String, BankTransaction> function = createMatchFunction();
        FunctionBasedReader<BankTransaction> reader = new FunctionBasedReader<>(function);
        ArrayList<BankTransaction> matchList = reader.read(matchFile);

        Assertions.assertEquals("", matchList.get(0).getReference());
    }

    @Test
    public void givenNullCurrencyCode_whenReadMatchFile_thenFail() throws IOException {

        Path matchFile = Paths.get(ClassLoader.getSystemResource("nullCurrency-match-file.csv").getPath());
        Function<String, BankTransaction> function = createMatchFunction();
        FunctionBasedReader<BankTransaction> reader = new FunctionBasedReader<>(function);
        ArrayList<BankTransaction> matchList = reader.read(matchFile);

        Assertions.assertEquals(" ", matchList.get(0).getCurrencyCode());
    }
    @Test
    public void givenValidMatchFile_whenReadMatchFile_thenPass() throws IOException {

        Path matchFile = Paths.get(ClassLoader.getSystemResource("match-file.csv").getPath());
        Function<String, BankTransaction> function = createMatchFunction();
        FunctionBasedReader<BankTransaction> reader = new FunctionBasedReader<>(function);
        ArrayList<BankTransaction> matchList = reader.read(matchFile);

        Assertions.assertEquals("TR-47884222201", matchList.get(0).getReference());
        Assertions.assertEquals("TR-47884222203", matchList.get(1).getReference());
        Assertions.assertEquals("TR-47884222206", matchList.get(2).getReference());

    }

    private Function<String, BankTransaction> createMatchFunction() {
        return (String line) -> {
            String[] transaction = line.split(",");

            String reference = transaction[0];
            String currencyCode = transaction[2];
            BigDecimal amount = BigDecimal.valueOf(Double.parseDouble(transaction[1]));
            Date date = convertToDate(transaction[3]);

            return new BankTransaction(reference, currencyCode, amount, date);
        };
    }

    private Date convertToDate(String strDate) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(strDate);
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
