package com.progressoft.jip8;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.Map;

public class CSVReaderTest {
    @Test
    public void givenNullCurrency_whenGetCurrencyCodeInCSV_thenFail() throws IOException, ParseException {
        Path csvFile = Paths.get(ClassLoader.getSystemResource("nullCurrency-file.csv").getPath());
        Reader<Map<String, BankTransaction>> reader = new CSVReader();
        Map<String, BankTransaction> csvFileTransaction = reader.read(csvFile);

        for (Map.Entry<String, BankTransaction> entry : csvFileTransaction.entrySet())
            Assertions.assertEquals("", entry.getValue().getCurrencyCode());
    }

    @Test
    public void givenNullReference_whenGetCurrencyCodeInCSV_thenFail() throws IOException, ParseException {
        Path csvFile = Paths.get(ClassLoader.getSystemResource("nullRef-file.csv").getPath());
        Reader<Map<String, BankTransaction>> reader = new CSVReader();
        Map<String, BankTransaction> csvFileTransaction = reader.read(csvFile);

        for (Map.Entry<String, BankTransaction> entry : csvFileTransaction.entrySet())
            Assertions.assertEquals("", entry.getKey());
    }

}
