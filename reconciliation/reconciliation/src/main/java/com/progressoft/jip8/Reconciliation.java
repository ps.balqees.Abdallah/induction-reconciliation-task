package com.progressoft.jip8;


import java.io.IOException;
import java.nio.file.Path;
import java.text.ParseException;

@FunctionalInterface
public interface Reconciliation<DATA> {

    Path reconcile(DATA request) throws IOException, ParseException;
}
