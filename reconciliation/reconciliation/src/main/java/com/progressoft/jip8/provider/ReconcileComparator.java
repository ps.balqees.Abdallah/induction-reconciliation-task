package com.progressoft.jip8.provider;

import com.progressoft.jip8.BankTransaction;
import com.progressoft.jip8.ResultHolder;
import com.progressoft.jip8.TransactionWithType;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;

public class ReconcileComparator implements Comparator {


    @Override
    public ResultHolder compare(Map<String, BankTransaction> sourceDataMap
                              , Map<String, BankTransaction> targetDataMap) {

        // TODO result list should be a local variable..Done
        ArrayList<BankTransaction> matchList = new ArrayList<>();
        ArrayList<Pair<BankTransaction, BankTransaction>> misMatchList = new ArrayList<>();
        ArrayList<TransactionWithType> missList = new ArrayList<>();

        validateInputs(sourceDataMap, targetDataMap);

        for (Map.Entry<String, BankTransaction> transaction : sourceDataMap.entrySet()) {

            BankTransaction bankTransaction = targetDataMap.get(transaction.getKey());
            ResultHolder resultHolder = new ResultHolder(matchList, misMatchList, missList);
            doComparing(targetDataMap, transaction, bankTransaction, resultHolder);

        }
        for (Map.Entry<String, BankTransaction> remainingTransaction : targetDataMap.entrySet()) {
            addToMissingTransactions("TARGET", remainingTransaction.getValue(), missList);
        }
        return new ResultHolder(matchList, misMatchList, missList);
    }

    private void doComparing(Map<String, BankTransaction> targetDataMap,
                             Map.Entry<String, BankTransaction> transaction, //sourceTransaction
                             BankTransaction bankTransaction,
                             ResultHolder resultHolder) {

        if (Objects.isNull(bankTransaction)) {
            addToMissingTransactions("SOURCE", transaction.getValue(), resultHolder.getMissList());//missing
            return;
        }
        if (!bankTransaction.equals(transaction.getValue())) {
            addToMisMatchingTransactions(transaction.getValue(), bankTransaction, resultHolder.getMisMatchList());//mismatching
            targetDataMap.remove(bankTransaction.getReference());
            return;
        } else {
            addToMatchingTransactions(bankTransaction, resultHolder.getMatchList());//matching
            targetDataMap.remove(bankTransaction.getReference());
        }
    }

    private void addToMissingTransactions(String foundInFile, BankTransaction bankTransaction, ArrayList<TransactionWithType> missList) {
        missList.add(new TransactionWithType(foundInFile, bankTransaction));
    }

    private void addToMatchingTransactions(BankTransaction bankTransaction, ArrayList<BankTransaction> matchList) {
        matchList.add(bankTransaction);
    }

    private void addToMisMatchingTransactions(BankTransaction sourceBankTransaction, BankTransaction targetBankTransaction, ArrayList<Pair<BankTransaction, BankTransaction>> misMatchList) {
        Pair<BankTransaction, BankTransaction> transactionPair =
                new ImmutablePair<>(sourceBankTransaction, targetBankTransaction);
        misMatchList.add(transactionPair);
    }

    private void validateInputs(Map<String, BankTransaction> sourceDataList, Map<String, BankTransaction> targetDataList) {
        throwIfNullSourceList(sourceDataList);
        throwIfNullTargetList(targetDataList);
    }

    private void throwIfNullTargetList(Map<String, BankTransaction> targetDataList) {
        if (Objects.isNull(targetDataList))
            throw new NullPointerException("Null targetMap");
    }

    private void throwIfNullSourceList(Map<String, BankTransaction> sourceDataList) {
        if (Objects.isNull(sourceDataList))
            throw new NullPointerException("Null sourceMap");
    }


}