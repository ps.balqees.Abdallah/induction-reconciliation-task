package com.progressoft.jip8.request;

public interface ReconcileRequest<DATA> {
    DATA getSourceFile();

    DATA getTargetFile();

    String getSourceExtension();

    String getTargetExtension();

    // TODO generic ..Done
    DATA getDirectory();
}
