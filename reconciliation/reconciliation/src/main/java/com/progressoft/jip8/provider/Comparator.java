package com.progressoft.jip8.provider;

import com.progressoft.jip8.BankTransaction;
import com.progressoft.jip8.ResultHolder;

import java.util.Map;

public interface Comparator {
    ResultHolder compare(Map<String, BankTransaction> sourceDataMap,
                         Map<String, BankTransaction> targetDataMap);
}
