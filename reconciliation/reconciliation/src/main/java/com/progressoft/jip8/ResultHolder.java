package com.progressoft.jip8;
import com.progressoft.jip8.BankTransaction;
import com.progressoft.jip8.TransactionWithType;
import org.apache.commons.lang3.tuple.Pair;
import java.util.ArrayList;

public class ResultHolder {

    private ArrayList<BankTransaction> matchList;
    private ArrayList<Pair<BankTransaction, BankTransaction>> misMatchList;
    private ArrayList<TransactionWithType> missList;

    public ResultHolder(ArrayList<BankTransaction> matchList,
                        ArrayList<Pair<BankTransaction, BankTransaction>> misMatchList,
                        ArrayList<TransactionWithType> missList) {
        this.matchList = matchList;
        this.misMatchList = misMatchList;
        this.missList = missList;
    }

    public ArrayList<BankTransaction> getMatchList() {
        return matchList;
    }

    public ArrayList<Pair<BankTransaction, BankTransaction>> getMisMatchList() {
        return misMatchList;
    }

    public ArrayList<TransactionWithType> getMissList() {
        return missList;
    }


}
