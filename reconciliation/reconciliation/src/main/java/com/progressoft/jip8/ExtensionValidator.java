package com.progressoft.jip8;

@FunctionalInterface
public interface ExtensionValidator {
    boolean isValidExtension(String extension);
}
