package com.progressoft.jip8.datasource;

import com.progressoft.jip8.ExtensionValidator;
import com.progressoft.jip8.provider.Comparator;
import com.progressoft.jip8.provider.ReconcileComparator;

public class DataSourceImplementation implements DataSource {

    @Override
    public ExtensionValidator getValidator() {
        return new ExtensionValidator() {
            @Override
            public boolean isValidExtension(String extension) {
                return true;
            }
        };
    }

    @Override
    public Comparator getComparator() {
        return new ReconcileComparator();
    }
}
