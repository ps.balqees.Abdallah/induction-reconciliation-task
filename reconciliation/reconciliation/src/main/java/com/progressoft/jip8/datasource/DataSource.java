package com.progressoft.jip8.datasource;

import com.progressoft.jip8.ExtensionValidator;
import com.progressoft.jip8.provider.Comparator;

public interface DataSource {
    ExtensionValidator getValidator();
    Comparator getComparator();
}
