package com.progressoft.jip8;

import com.progressoft.jip8.datasource.DataSource;
import com.progressoft.jip8.provider.Comparator;
import com.progressoft.jip8.request.ReconcileRequest;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;

public class SimpleReconciliation implements Reconciliation<ReconcileRequest<Path>> {

    private final ExtensionValidator validator;
    private final Comparator comparator;
    private MatchDataWriter matchDataWriter = new MatchDataWriter();
    private MismatchDataWriter mismatchDataWriter = new MismatchDataWriter();
    private MissDataWriter missDataWriter = new MissDataWriter();


    public SimpleReconciliation(DataSource dataSource) {
        throwIfNullDataSource(dataSource);
        this.validator = dataSource.getValidator();
        this.comparator = dataSource.getComparator();
    }

    @Override
    public Path reconcile(ReconcileRequest<Path> request) throws IOException, ParseException {
        isValidData(request.getSourceFile(), request.getSourceExtension(), "source");
        isValidData(request.getTargetFile(), request.getTargetExtension(), "target");

        Map<String, BankTransaction> sourceTransactionMap = readFile(request.getSourceFile(), request.getSourceExtension());
        Map<String, BankTransaction> targetTransactionMap = readFile(request.getTargetFile(), request.getTargetExtension());

        Path directory = request.getDirectory();
        createDirIfNotExist(directory);

        return doReconcile(sourceTransactionMap, targetTransactionMap, directory);
    }

    private Path doReconcile(Map<String, BankTransaction> sourceList
            , Map<String, BankTransaction> targetList, Path directory) throws IOException {

//TODO you should move the whole compare process inside the provider

//TODO rename the provider to Comparator then let it returns an object holding the results..Done

        ResultHolder resultHolder = comparator.compare(sourceList, targetList);

        dataWriter(matchDataWriter, "match-file.csv",
                directory, resultHolder.getMatchList());

        dataWriter(mismatchDataWriter, "mismatch-file.csv",
                directory, resultHolder.getMisMatchList());


        dataWriter(missDataWriter, "miss-file.csv",
                directory, resultHolder.getMissList());

        return directory;
    }

    private void createDirIfNotExist(Path directory) throws IOException {
        if (!Files.exists(directory))
            Files.createDirectory(directory);
    }

    private Map<String, BankTransaction> readFile(Path filePath, String fileExtension) throws IOException, ParseException {
        Reader<Map<String, BankTransaction>> fileReader = ReaderFactory.getReader(fileExtension);
        return fileReader.read(filePath);
    }

    private <T> void dataWriter(Writer<T> writer, String fileName, Path directory, ArrayList<T> data) throws IOException {
        Path path = Paths.get(directory.toString(), fileName);
        writer.write(data, path);
    }

    private void isValidData(Path filePath, String fileExtension, String type) {
        if (Objects.isNull(filePath))
            throw new NullFileException("null " + type + " file");
        if (Objects.isNull(fileExtension))
            throw new NullExtensionException("null " + type + " extension");
        if (!validator.isValidExtension(fileExtension))
            throw new IllegalArgumentException("invalid " + type + " extension");
    }

    private void throwIfNullDataSource(DataSource dataSource) {
        if (Objects.isNull(dataSource))
            throw new NullPointerException("Null dataSource");
    }
}
