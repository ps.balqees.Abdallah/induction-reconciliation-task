package com.progressoft.jip8.request;

import java.nio.file.Path;

public class AppRequest implements ReconcileRequest<Path> {
    private Path sourceFile;
    private Path targetFile;
    private String sourceExtension;
    private String targetExtension;
    private Path directory;

    public AppRequest(Path sourceFile, Path targetFile, String sourceExtension, String targetExtension, Path directory) {
        this.sourceFile = sourceFile;
        this.targetFile = targetFile;
        this.sourceExtension = sourceExtension;
        this.targetExtension = targetExtension;
        this.directory = directory;
    }


    public Path getSourceFile() {
        return sourceFile;
    }

    public Path getTargetFile() {
        return targetFile;
    }

    public String getSourceExtension() {
        return sourceExtension;
    }

    public String getTargetExtension() {
        return targetExtension;
    }

    public Path getDirectory() {
        return directory;
    }
}
