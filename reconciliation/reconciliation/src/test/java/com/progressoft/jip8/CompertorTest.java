package com.progressoft.jip8;

import com.progressoft.jip8.provider.ReconcileComparator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CompertorTest {
    @Test
    public void givenNullSourceMap_whenGetMatchedData_thenThrowNullPointerException() {
        ReconcileComparator compertor = new ReconcileComparator();
        Map<String, BankTransaction> sourceMap = null;
        Map<String, BankTransaction>targetMap= new HashMap<>();
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                () -> compertor.compare(sourceMap,targetMap));
        Assertions.assertEquals("Null sourceMap", exception.getMessage());

    }

    @Test
    public void givenNullTargetList_whenGetMatchedData_thenThrowNullPointerException() {
        ReconcileComparator compertor = new ReconcileComparator();
        HashMap<String,BankTransaction> sourceDataList = new HashMap<>();
        HashMap<String,BankTransaction> targetDataList = null;
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                () -> compertor.compare(sourceDataList, targetDataList));
        Assertions.assertEquals("Null targetMap", exception.getMessage());

    }


}
