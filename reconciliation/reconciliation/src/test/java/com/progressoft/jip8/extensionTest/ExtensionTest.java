package com.progressoft.jip8.extensionTest;

import com.progressoft.jip8.ReaderFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ExtensionTest {

    @Test
    public void givenNullExtension_whenReaderFactory_thenThrowNullPointerException() {
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class,
                () -> ReaderFactory.getReader(null));
        Assertions.assertEquals("Null extension", exception.getMessage());
    }

    @Test
    public void givenInvalidExtension_whenReaderFactory_thenThrowIllegalArgumentException() {
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> ReaderFactory.getReader("ABC"));
        Assertions.assertEquals("Invalid extension", exception.getMessage());
    }

}
