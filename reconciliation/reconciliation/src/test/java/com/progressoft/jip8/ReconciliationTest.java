package com.progressoft.jip8;
import com.progressoft.jip8.datasource.DataSource;
import com.progressoft.jip8.datasource.DataSourceImplementation;
import com.progressoft.jip8.request.AppRequest;
import com.progressoft.jip8.request.ReconcileRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

public class ReconciliationTest {

    @Test
    public void givenNullDataSource_whenReconcile_thenThrowNullPointerException() {
        DataSource dataSource = null;
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class
                , () -> new SimpleReconciliation(dataSource));
        Assertions.assertEquals("Null dataSource", exception.getMessage());
    }

    @Test
    public void givenNullSourceFile_whenReconcile_thenThrowNullPointerException() {
        DataSource dataSource=new DataSourceImplementation();
        Reconciliation reconciliation = new SimpleReconciliation(dataSource);

        Path sourceFile = null;
        String sourceExtension = "csv";
        Path targetFile = Paths.get(System.getProperty(".", "bank-transaction.json"));
        String targetExtension = "json";

        Path directory = Paths.get(System.getProperty("user.home") + "/resultFiles");

        ReconcileRequest request = new AppRequest(sourceFile, targetFile, sourceExtension, targetExtension, directory);
        NullFileException exception = Assertions.assertThrows(NullFileException.class
                , () -> reconciliation.reconcile(request));
        Assertions.assertEquals("null source file", exception.getMessage());
    }

    @Test
    public void givenNullTargetFile_whenReconcile_thenThrowNullPointerException() {
        DataSource dataSource=new DataSourceImplementation();
        Reconciliation reconciliation = new SimpleReconciliation(dataSource);
        Path sourceFile = Paths.get(System.getProperty(".", "bank-transaction.csv"));
        String sourceExtension = "csv";
        Path targetFile = null;
        String targetExtension = "csv";
        Path directory = Paths.get(System.getProperty("user.home") + "/resultFiles");

        ReconcileRequest request = new AppRequest(sourceFile, targetFile, sourceExtension, targetExtension, directory);

        NullFileException exception = Assertions.assertThrows(NullFileException.class
                , () -> reconciliation.reconcile(request));

        Assertions.assertEquals("null target file", exception.getMessage());
    }

    @Test
    public void givenNullSourceExtension_whenReconcile_thenThrowNullPointerException() {
        DataSource dataSource=new DataSourceImplementation();
        Reconciliation reconciliation = new SimpleReconciliation(dataSource);

        Path sourceFile = Paths.get(System.getProperty(".", "bank-transaction.csv"));
        String sourceExtension = null;
        Path targetFile = Paths.get(System.getProperty(".", "bank-transaction.json"));
        String targetExtension = "csv";
        Path directory = Paths.get(System.getProperty("user.home") + "/resultFiles");

        ReconcileRequest request = new AppRequest(sourceFile, targetFile, sourceExtension, targetExtension, directory);

        NullExtensionException exception = Assertions.assertThrows(NullExtensionException.class
                , () -> reconciliation.reconcile(request));
        Assertions.assertEquals("null source extension", exception.getMessage());
    }

    @Test
    public void givenNullTargetExtension_whenReconcile_thenThrowNullPointerException() {
        DataSource dataSource=new DataSourceImplementation();
        Reconciliation reconciliation = new SimpleReconciliation(dataSource);

        Path sourceFile = Paths.get(System.getProperty(".", "bank-transaction.csv"));
        String sourceExtension = "csv";

        Path targetFile = Paths.get(System.getProperty(".", "bank-transaction.json"));
        String targetExtension = null;
        Path directory = Paths.get(System.getProperty("user.home") + "/resultFiles");

        ReconcileRequest request = new AppRequest(sourceFile, targetFile, sourceExtension, targetExtension, directory);

        NullExtensionException exception = Assertions.assertThrows(NullExtensionException.class
                , () -> reconciliation.reconcile(request));
        Assertions.assertEquals("null target extension", exception.getMessage());
    }


    @Test
    public void givenInvalidSourceExtensionAndValidTargetExtension_whenReconcile_thenThrowIllegalArgumentException() {
        DataSource dataSource=new DataSourceImplementation();
        Reconciliation reconciliation = new SimpleReconciliation(dataSource);

        Path sourceFile = Paths.get(System.getProperty(".", "bank-transaction.csv"));
        String sourceExtension = "abc";

        Path targetFile = Paths.get(System.getProperty(".", "bank-transaction.json"));
        String targetExtension = "json";

        Path directory = Paths.get(System.getProperty("user.home") + "/resultFiles");

        ReconcileRequest request = new AppRequest(sourceFile, targetFile, sourceExtension, targetExtension, directory);

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class
                , () -> reconciliation.reconcile(request));
        Assertions.assertEquals("Invalid extension", exception.getMessage());
    }

    @Test
    public void givenInValidSourceExtensionInvalidTargetExtension_whenReconcile_thenThrowIllegalArgumentException() {
        DataSource dataSource=new DataSourceImplementation();
        Reconciliation reconciliation = new SimpleReconciliation(dataSource);

        Path sourceFile = Paths.get(System.getProperty(".", "bank-transaction.csv"));
        String sourceExtension = "abc";

        Path targetFile = Paths.get(System.getProperty(".", "bank-transaction.json"));
        String targetExtension = "abc";
        Path directory = Paths.get(System.getProperty("user.home") + "/resultFiles");

        ReconcileRequest request = new AppRequest(sourceFile, targetFile, sourceExtension, targetExtension, directory);

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class
                , () -> reconciliation.reconcile(request));
        Assertions.assertEquals("Invalid extension", exception.getMessage());
    }

    // TODO separate test cases for reconciliation from reader ... Done
}
