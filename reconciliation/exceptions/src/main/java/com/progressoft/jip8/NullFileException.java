package com.progressoft.jip8;

public class NullFileException extends NullPointerException {
    public NullFileException(String message) {
        super(message);
    }
}
