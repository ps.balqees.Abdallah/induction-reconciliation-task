package com.progressoft.jip8;

public class NullExtensionException extends NullPointerException {
    public NullExtensionException(String message) {
        super(message);
    }
}
