package com.progressoft.jip8;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class MissWriterTest {

    @Test
    public void givenNullResultList_whenWriteMissData_thenThrowNullPointerException()  {
        Writer<TransactionWithType> writer = new MissDataWriter();
        ArrayList<TransactionWithType> resultList = null;
        Path file = Paths.get(ClassLoader.getSystemResource("miss-file.csv").getPath());
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class
                , () -> writer.write(resultList, file));
        Assertions.assertEquals("Null result list", exception.getMessage());
    }

    @Test
    public void givenNullFile_whenWriteMissData_thenThrowNullPointerException() {
        Writer<TransactionWithType> writer = new MissDataWriter();
        ArrayList<TransactionWithType> resultList = new ArrayList<>();
        Path file = null;
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class
                , () -> writer.write(resultList, file));
        Assertions.assertEquals("Null file", exception.getMessage());
    }
}
