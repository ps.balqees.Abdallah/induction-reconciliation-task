package com.progressoft.jip8;

import org.apache.commons.lang3.tuple.Pair;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

public class MismatchDataWriter extends Writer<Pair<BankTransaction, BankTransaction>> {

    @Override
    public void write(ArrayList<Pair<BankTransaction, BankTransaction>> resultList, Path file) throws IOException {

        throwIfNullResultList(resultList, "Null result list");
        throwIfNullFile(file, "Null file");

        try (FileWriter writer = new FileWriter(String.valueOf(file))) {
            writeHeaderFoundInFile(writer);
            writeHeaderInResultFile(writer);
            for (int i = 0; i < resultList.size(); i++) {
                writeTransaction(writer, "SOURCE", resultList.get(i).getKey());
                writeTransaction(writer, "TARGET", resultList.get(i).getKey());
            }
        }
    }

    private void writeTransaction(FileWriter writer, String foundInFile, BankTransaction bankTransaction) throws IOException {
        writer.write(foundInFile);
        writer.write(",");
        printBankTransaction(bankTransaction, writer);
    }

}