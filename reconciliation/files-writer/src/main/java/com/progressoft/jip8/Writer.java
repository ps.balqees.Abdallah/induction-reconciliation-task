package com.progressoft.jip8;


import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

public abstract class Writer<T> {

    public abstract void write(ArrayList<T> resultList, Path file) throws IOException;

    public void throwIfNullResultList(ArrayList<T> resultList, String message) {
        if (Objects.isNull(resultList))
            throw new NullPointerException(message);
    }

    public void throwIfNullFile(Path file, String message) {
        if (Objects.isNull(file))
            throw new NullPointerException(message);
    }

    public void writeHeaderInResultFile(FileWriter writer) throws IOException {
        writer.write("transaction id");
        writer.write(",");
        writer.write("amount");
        writer.write(",");
        writer.write("currency code");
        writer.write(",");
        writer.write("value date");
        writer.write("\n");
        writer.flush();
    }

    public void writeHeaderFoundInFile(FileWriter writer) throws IOException {
        writer.write("found in file");
        writer.write(",");
        writer.flush();
    }

    public void printBankTransaction(BankTransaction bankTransaction, FileWriter writer) throws IOException {
        Date date = bankTransaction.getDate();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String format = dateFormat.format(date);

        writer.write(bankTransaction.getReference());
        writer.write(",");
        writer.write(bankTransaction.getAmount() + "");
        writer.write(",");
        writer.write(bankTransaction.getCurrencyCode());
        writer.write(",");
        writer.write(format);
        writer.write("\n");
        writer.flush();
    }
}