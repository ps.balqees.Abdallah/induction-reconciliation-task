package com.progressoft.jip8;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

public class MatchDataWriter extends Writer<BankTransaction> {

    @Override
    public void write(ArrayList<BankTransaction> resultList, Path file) throws IOException {
        throwIfNullResultList(resultList, "Null result list");
        throwIfNullFile(file, "Null file");
        try (FileWriter writer = new FileWriter(String.valueOf(file))) {
            writeHeaderInResultFile(writer);
            for (int i = 0; i < resultList.size(); i++) {
                printBankTransaction(resultList.get(i), writer);
            }
        }
    }
}