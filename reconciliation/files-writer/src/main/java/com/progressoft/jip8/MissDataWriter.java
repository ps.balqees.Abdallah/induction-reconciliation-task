package com.progressoft.jip8;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

public class MissDataWriter extends Writer<TransactionWithType> {

    @Override
    public void write(ArrayList<TransactionWithType> resultList, Path file) throws IOException {

        throwIfNullResultList(resultList, "Null result list");
        throwIfNullFile(file, "Null file");

        try (FileWriter writer = new FileWriter(String.valueOf(file))) {
            writeHeaderFoundInFile(writer);
            writeHeaderInResultFile(writer);
            for (int i = 0; i < resultList.size(); i++) {
                writer.write(resultList.get(i).getType());
                writer.write(",");
                printBankTransaction(resultList.get(i).getBankTransaction(), writer);
            }
        }
    }

}