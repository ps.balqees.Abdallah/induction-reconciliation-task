package com.progressoft.jip8.web;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Objects;

public class DownloadServlet extends HttpServlet {

    @Override
    public void init() {
        System.out.println("init was called");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            String fileName = request.getParameter("fileName");
            String fileType = request.getParameter("fileType");

            // TODO this would be reported to the browser as an internal error 500 not as bad request
            throwIfNullFileData(fileName, response);
            throwIfNullFileData(fileType, response);

            Path resultDirectory = (Path) request.getSession().getAttribute("directory");
            Path filePath = Paths.get(resultDirectory.toString(), fileName + "-file.csv");

            try (InputStream inputStream = Files.newInputStream(filePath);
                 OutputStream out = response.getOutputStream()) {

                response.setContentType("text/csv");
                response.setHeader("Content-disposition", "attachment; filename="
                        + fileName + "." + fileType);

                if (isCsv(fileType)) {
                    writeAsCsv(inputStream, out);
                } else if (isJson(fileType)) {
                    writeAsJson(inputStream, out);
                } else
                    throw new IllegalArgumentException("not provided format type!");
            }
        } catch (Exception ex) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, ex.getMessage());
        }
    }

    private boolean isJson(String fileType) {
        return fileType.equalsIgnoreCase("json");
    }

    private boolean isCsv(String fileType) {
        return fileType.equalsIgnoreCase("csv");
    }

    private void writeAsJson(InputStream inputStream, OutputStream out) throws IOException {
        String jsonString = convertCsvToJson(inputStream);
        out.write(jsonString.getBytes());
    }

    private String convertCsvToJson(InputStream inputStream) throws IOException {
        // TODO close streams ... Done
        try (InputStreamReader inputStreamReader = new InputStreamReader(inputStream)) {
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String[] headers = bufferedReader.readLine().split(",");
            String line;
            ArrayList<String> jsonObjects = new ArrayList<>();

            while ((line = bufferedReader.readLine()) != null) {
                String[] lineObject = line.split(",");
                jsonObjects.add(convertCsvLineToJsonObject(headers, lineObject));
            }
            StringBuilder stringBuilder = getStringBuilder(jsonObjects);
            return stringBuilder.toString();
        }
    }

    private StringBuilder getStringBuilder(ArrayList<String> jsonObjects) {
        StringBuilder stringBuilder = new StringBuilder();
        if (isValidArraySize(jsonObjects)) {
            stringBuilder.append("[");
            for (int i = 0; i < jsonObjects.size(); i++) {
                stringBuilder.append(jsonObjects.get(i));
                stringBuilder.append(",");
            }
            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            stringBuilder.append("]");
        }
        return stringBuilder;
    }

    private boolean isValidArraySize(ArrayList<String> jsonObjects) {
        return jsonObjects.size() > 0;
    }

    private String convertCsvLineToJsonObject(String[] headers, String[] lineObject) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("{");
        for (int i = 0; i < headers.length; i++) {
            stringBuilder.append("\"" + headers[i] + "\"" + ":" + "\"" + lineObject[i] + "\"");
            stringBuilder.append(",");
        }
        stringBuilder.deleteCharAt(stringBuilder.length() - 1); //deleting "," from last index
        stringBuilder.append("}");
        return stringBuilder.toString();
    }

    private void writeAsCsv(InputStream inputStream, OutputStream out) throws IOException {
        byte[] buffer = new byte[1048];
        int numBytesRead;
        while ((numBytesRead = inputStream.read(buffer)) > 0) {
            out.write(buffer, 0, numBytesRead);
        }
    }

    private void throwIfNullFileData(String data, HttpServletResponse response) throws IOException {
        if (Objects.isNull(data)) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Null File Name");
            return;
        }
    }

    @Override
    public void destroy() {
        System.out.println(" Servlet out of service.");
    }
}
