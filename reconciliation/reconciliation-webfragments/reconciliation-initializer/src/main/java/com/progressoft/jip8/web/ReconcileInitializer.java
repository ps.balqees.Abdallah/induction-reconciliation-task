package com.progressoft.jip8.web;

import com.progressoft.jip8.*;
import com.progressoft.jip8.datasource.DataSourceImplementation;

import javax.servlet.*;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.function.Function;

public class ReconcileInitializer implements ServletContainerInitializer {

    public void onStartup(Set<Class<?>> set, ServletContext ctx) {
        registerLogin(ctx);
        registerSourceFileUpload(ctx);
        registerTargetFileUpload(ctx);
        registerSummary(ctx);
        registerReconciliation(ctx);
        registerDownload(ctx);
        registerFilter(ctx);
    }

    private void registerDownload(ServletContext ctx) {
        DownloadServlet servlet = new DownloadServlet();
        ServletRegistration.Dynamic registration
                = ctx.addServlet("DownloadServlet", servlet);
        registration.addMapping("/download");
    }


    private void registerFilter(ServletContext servletContext) {
        LoginFilter loginFilter = new LoginFilter();
        FilterRegistration.Dynamic registration = servletContext.addFilter("LoginFilter", loginFilter);
        registration.addMappingForServletNames(null, true, "SourceFileUploadServlet");
        registration.addMappingForServletNames(null, true, "TargetFileUploadServlet");
        registration.addMappingForServletNames(null, true, "SummaryServlet");
        registration.addMappingForServletNames(null, true, "ReconcileServlet");
        registration.addMappingForServletNames(null, true, "DownloadServlet");
    }

    private void registerReconciliation(ServletContext servletContext) {
        final Reconciliation reconciliation = new SimpleReconciliation(new DataSourceImplementation());
        final Function<String, BankTransaction> matchFunction = createMatchFunction();
        final Function<String, TransactionWithType> missFunction = createMissingAndMismatchFunction();

        FunctionBasedReader<BankTransaction> matchReader = new FunctionBasedReader<>(matchFunction);
        FunctionBasedReader<TransactionWithType> missReader = new FunctionBasedReader<>(missFunction);

        ReconcileServlet servlet = new ReconcileServlet(reconciliation, missReader, matchReader);
        ServletRegistration.Dynamic registration
                = servletContext.addServlet("ReconcileServlet", servlet);
        registration.addMapping("/reconcile");
    }

    private Function<String, TransactionWithType> createMissingAndMismatchFunction() {
        return (String line) -> {
            String[] transaction = line.split(",");

            String type = transaction[0];
            String reference = transaction[1];
            String currencyCode = transaction[3];
            BigDecimal amount = BigDecimal.valueOf(Double.parseDouble(transaction[2]));
            Date date = convertToDate(transaction[4]);

            BankTransaction bankTransaction = new BankTransaction(reference, currencyCode, amount, date);
            return new TransactionWithType(type, bankTransaction);
        };
    }

    private Function<String, BankTransaction> createMatchFunction() {
        return (String line) -> {
            String[] transaction = line.split(",");

            String reference = transaction[0];
            String currencyCode = transaction[2];
            BigDecimal amount = BigDecimal.valueOf(Double.parseDouble(transaction[1]));
            Date date = convertToDate(transaction[3]);

            return new BankTransaction(reference, currencyCode, amount, date);
        };
    }

    private Date convertToDate(String strDate) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(strDate);
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private void registerSummary(ServletContext servletContext) {
        SummaryServlet servlet = new SummaryServlet();
        ServletRegistration.Dynamic registration
                = servletContext.addServlet("SummaryServlet", servlet);
        registration.addMapping("/summary");
    }

    private void registerTargetFileUpload(ServletContext servletContext) {
        String saveDir = System.getProperty("targetFile");
        String routePath = "/summary";
        String servletName = "TargetFileUploadServlet";
        String mapping = "/targetUpload";
        FileUploadData fileUploadData = new FileUploadData(saveDir, routePath, servletName, mapping);
        registerFileUpload(servletContext, fileUploadData);
    }

    private void registerSourceFileUpload(ServletContext servletContext) {
        String saveDir = System.getProperty("sourceFile");
        String routePath = "/targetUpload";
        String servletName = "SourceFileUploadServlet";
        String mapping = "/sourceUpload";
        FileUploadData fileUploadData = new FileUploadData(saveDir, routePath, servletName, mapping);
        registerFileUpload(servletContext, fileUploadData);
    }

    private void registerFileUpload(ServletContext servletContext, FileUploadData fileUploadData) {

        UploadServlet servlet = new UploadServlet(fileUploadData.getRoutePath(), fileUploadData.getSaveDir());
        ServletRegistration.Dynamic registration
                = servletContext.addServlet(fileUploadData.getServletName(), servlet);
        registration.setMultipartConfig(new MultipartConfigElement("file/tmp"));
        registration.addMapping(fileUploadData.getMapping());
    }

    private void registerLogin(ServletContext ctx) {
        LoginServlet servlet = new LoginServlet();
        ServletRegistration.Dynamic registration
                = ctx.addServlet("LoginServlet", servlet);
        registration.addMapping("/login");
    }

    private class FileUploadData {
        private String saveDir;
        private String routePath;
        private String servletName;
        private String mapping;

        public FileUploadData(String saveDir, String routePath, String servletName, String mapping) {
            this.saveDir = saveDir;
            this.routePath = routePath;
            this.servletName = servletName;
            this.mapping = mapping;
        }

        public String getSaveDir() {
            return saveDir;
        }

        public String getRoutePath() {
            return routePath;
        }

        public String getServletName() {
            return servletName;
        }

        public String getMapping() {
            return mapping;
        }

    }

}

