<%@attribute name="label" required="true"%>
<%@attribute name="fileName"  required="true"%>
<%@attribute name="fileType"  required="true"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="row">
     <div class="col-6">
       <div class="card" style="width: 18rem;">
         <div class="card-header">
             <p class="card-text">${label}</p>
         </div>
         <div class="card-body">
             <label class="card-text">Name : ${fileName}</label>
             <br>
             <label class="card-text">Type : ${fileType}</label>
         </div>
       </div>
</div>