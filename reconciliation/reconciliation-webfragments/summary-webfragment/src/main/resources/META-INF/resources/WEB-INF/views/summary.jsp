<%@taglib prefix="jip" tagdir="/WEB-INF/tags" %>

<div class="container">
<form action="${pageContext.request.contextPath}/reconcile" enctype='multipart/form-data'
                                                            method="POST">
  <div class="row">
    <div class="col-12">
      <jip:summary-file-name-and-type  fileName="${requestScope.summaryResponse.sourceFileSummary.fileName}"
                           fileType= "${requestScope.summaryResponse.sourceFileSummary.fileType}"
                           label="Source"/>
      <jip:summary-file-name-and-type  fileName="${requestScope.summaryResponse.targetFileSummary.fileName}"
                           fileType= "${requestScope.summaryResponse.targetFileSummary.fileType}"
                           label="Target"/>
    </div>

    <div class="col-12">
       <p >Result Files Format :</p>
       <select name="resultType"  id="resultType" style="width:50%">
       <option name="csv"  id="csv">csv</option>
       <option name="json" id="json">json</option>
       </select>
    </div>
    <div class="col-12">
    <br><br>
    <button class="btn btn-secondary"><a  style="text-decoration:none;color:white"
                href="${pageContext.request.contextPath}/targetUpload"> Back</a>
    </button>
    <button class="btn btn-secondary"><a style="text-decoration:none;color:white"
               href="${pageContext.request.contextPath}/sourceUpload">Cancel</a>
    </button>
    <button  id="" type="submit" class="btn btn-primary">Compare</button>

    </div>
  </div>
</form>
</div>
