package com.progressoft.jip8.web;

public class SummaryResponse {

    private FileSummary sourceFileSummary;
    private FileSummary targetFileSummary;

    public FileSummary getSourceFileSummary() {
        return sourceFileSummary;
    }

    public void setSourceFileSummary(FileSummary sourceFileSummary) {
        this.sourceFileSummary = sourceFileSummary;
    }

    public FileSummary getTargetFileSummary() {
        return targetFileSummary;
    }

    public void setTargetFileSummary(FileSummary targetFileSummary) {
        this.targetFileSummary = targetFileSummary;
    }
}
