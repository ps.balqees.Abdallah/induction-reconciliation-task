package com.progressoft.jip8.web;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
public class SummaryServlet extends HttpServlet {

    @Override
    public void init() {
        System.out.println("init was called");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        SummaryResponse summaryResponse = new SummaryResponse();

        FileSummary sourceFileSummary = (FileSummary) req.getSession().getAttribute("sourceSummery");
        FileSummary targetFileSummary = (FileSummary) req.getSession().getAttribute("targetSummery");

        summaryResponse.setSourceFileSummary(sourceFileSummary);
        summaryResponse.setTargetFileSummary(targetFileSummary);

        req.setAttribute("summaryResponse", summaryResponse);
        req.getSession().setAttribute("summaryResponse", summaryResponse);
        RequestDispatcher requestDispatcher
                = req.getRequestDispatcher("/WEB-INF/views/summary.jsp");

        requestDispatcher.forward(req, resp);
    }

//    @Override
//    // TODO this will never be called
//    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
//        response.setContentType("text/html");
//        response.sendRedirect(request.getContextPath() + "/reconcile");
//    }

    @Override
    public void destroy() {
        System.out.println(" Servlet out of service.");
    }
}
