//package com.progressoft.jip8.web;
//
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.Test;
//
//public class LoginServletTest {
//    @Test
//    public void givenNullUsername_whenGetUsername_thenThrowNullPointerException() {
//        NullPointerException exception = Assertions.assertThrows(NullPointerException.class
//                , () -> new LoginServlet(null, "admin"));
//        Assertions.assertEquals("null username", exception.getMessage());
//    }
//
//    @Test
//    public void givenNullPassword_whenGetPassword_thenThrowNullPointerException() {
//        NullPointerException exception = Assertions.assertThrows(NullPointerException.class
//                , () -> new Login("admin", null));
//        Assertions.assertEquals("null password", exception.getMessage());
//    }
//
//    @Test
//    public void givenNullUsernameAndPassword_whenGetUsernamePassword_thenThrowNullPointerException() {
//        NullPointerException exception = Assertions.assertThrows(NullPointerException.class
//                , () -> new Login(null, null));
//        Assertions.assertEquals("null username and password", exception.getMessage());
//    }
//
//    @Test
//    public void givenInValidUsernameAndValidPassword_whenGetUsernamePassword_thenFail() {
//        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class
//                , () -> new Login("abc", "admin"));
//        Assertions.assertEquals("Invalid username", exception.getMessage());
//    }
//
//    @Test
//    public void givenValidUsernameAndInValidPassword_whenGetUsernamePassword_thenFail() {
//        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class
//                , () -> new Login("admin", "abc"));
//        Assertions.assertEquals("Invalid password", exception.getMessage());
//    }
//
//    @Test
//    public void givenInValidUsernameAndInValidPassword_whenGetUsernamePassword_thenFail() {
//        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class
//                , () -> new Login("abc", "abc"));
//        Assertions.assertEquals("Invalid username and password", exception.getMessage());
//    }
//
//    @Test
//    public void givenValidUsernameAndPassword_whenGetUsernamePassword_thenPass() {
//        Login login = Assertions.assertDoesNotThrow(
//                () -> new Login("admin", "admin"));
//        Assertions.assertEquals("admin" , login.getUsername());
//        Assertions.assertEquals("admin" , login.getPassword());
//
//    }
//}
