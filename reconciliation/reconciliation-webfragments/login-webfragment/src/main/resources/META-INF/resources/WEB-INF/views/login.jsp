<%@taglib prefix="jip" tagdir="/WEB-INF/tags" %>

<div class="container">
    <form action="${pageContext.request.contextPath}/login" method="POST">
      <div class="form-group">
            <jip:username-password-input label="username"
                                         type="name"
                                         placeholder="Enter name"/>
            <small class="form-text text-muted"> We'll never share your email with anyone else.</small>
      </div>
      <div class="form-group">
            <jip:username-password-input label="password"
                                         type="password"
                                         placeholder="Enter password"/>
      </div>
      <button type="submit" class="btn btn-primary">Login</button>
    </form>
</div>

