<%@attribute name="label" required="true"%>
<%@attribute name="type" required="true"%>
<%@attribute name="placeholder" required="true"%>


<label for="${label}">${label}</label>

        <input type="${type}"
               class="form-control"
               id="${label}"
               name="${label}"
               placeholder="${placeholder}"
               required>