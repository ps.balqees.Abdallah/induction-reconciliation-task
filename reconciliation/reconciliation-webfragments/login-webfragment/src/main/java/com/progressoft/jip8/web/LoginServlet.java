package com.progressoft.jip8.web;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginServlet extends HttpServlet {

    // TODO those should be local variables ... Done

    @Override
    public void init() {
        // TODO useless login ..Done
        System.out.println("init was called");
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String error = (String) session.getAttribute("loginFail");
        req.setAttribute("loginFail", error);

        RequestDispatcher requestDispatcher
                = req.getRequestDispatcher("/WEB-INF/views/login.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String username;
        String password;
        boolean loggedIn;
        response.setContentType("text/html");
        try {
            username = request.getParameter("username");
            password = request.getParameter("password");

            if(username.equals("admin")&&password.equals("admin"))
                request.getSession().setAttribute("loggedIn",true);

//            request.getSession().setAttribute("username", username);
//            request.getSession().setAttribute("password", password);

            response.sendRedirect(request.getContextPath() + "/sourceUpload");
        } catch (Exception ex) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, ex.getMessage());
        }
    }

    @Override
    public void destroy() {
        System.out.println(" Servlet out of service.");
    }

}
