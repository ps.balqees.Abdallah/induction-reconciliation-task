package com.progressoft.jip8.web;


import java.util.Objects;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LoginFilter implements Filter {
    // TODO those should be local variables ... Done

    Logger logger = Logger.getLogger(LoginFilter.class.getName());

    @Override
    public void init(FilterConfig filterConfig) {
        logger.warning("filter ");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;

        Object loggedIn =req.getSession().getAttribute("loggedIn");
        if( Objects.isNull(loggedIn) ||   (Boolean)loggedIn!=true){
            resp.sendRedirect(req.getContextPath() +"/login");
            return;
        }
        filterChain.doFilter(req, resp);
 }
    @Override
    public void destroy() {
    }
}
