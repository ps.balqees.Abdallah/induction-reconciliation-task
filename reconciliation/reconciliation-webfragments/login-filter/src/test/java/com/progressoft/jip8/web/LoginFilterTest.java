//package com.progressoft.jip8.web;
//
//import org.junit.jupiter.api.Test;
//
//import javax.servlet.FilterChain;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//import java.io.IOException;
//
//import org.mockito.Mockito;
//
//import static org.mockito.Mockito.*;
//
//public class LoginFilterTest {
//
//    @Test
//    public void givenInvalidUsernameAndPassword_whenLogin_thenRedirect() throws IOException, ServletException {
//        HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
//        HttpServletResponse httpServletResponse = mock(HttpServletResponse.class);
//        HttpSession session = mock(HttpSession.class);
//
//        when(httpServletRequest.getContextPath()).thenReturn("/app");
//        when(session.getAttribute("username")).thenReturn("admin1");
//        when(session.getAttribute("password")).thenReturn("admin");
//        when(httpServletRequest.getSession()).thenReturn(session);
//
//        FilterChain filterChain = mock(FilterChain.class);
//
//        LoginFilter loginFilter = new LoginFilter();
//        loginFilter.doFilter(httpServletRequest, httpServletResponse,
//                filterChain);
//        verify(httpServletResponse).sendRedirect("/app/login");
//    }
//
//    @Test
//    public void givenEmptyUsernameAndPassword_whenLogin_thenShouldRedirect() throws IOException, ServletException {
//        HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
//        HttpServletResponse httpServletResponse = mock(HttpServletResponse.class);
//
//        when(httpServletRequest.getContextPath()).thenReturn("/app");
//        HttpSession session = mock(HttpSession.class);
//
//        when(session.getAttribute("username")).thenReturn("");
//        when(session.getAttribute("password")).thenReturn("");
//        when(httpServletRequest.getSession()).thenReturn(session);
//
//        FilterChain filterChain = mock(FilterChain.class);
//
//        LoginFilter loginFilter = new LoginFilter();
//        loginFilter.doFilter(httpServletRequest, httpServletResponse,
//                filterChain);
//        verify(httpServletResponse).sendRedirect("/app/login");
//    }
//
//    @Test
//    public void givenValidUserNameAndPassword_whenLogin_thenShouldForward() throws IOException, ServletException {
//        HttpServletRequest httpServletRequest = mock(HttpServletRequest.class);
//        HttpServletResponse httpServletResponse = mock(HttpServletResponse.class);
//
//        HttpSession session = mock(HttpSession.class);
//
//        when(session.getAttribute("username")).thenReturn("admin");
//        when(session.getAttribute("password")).thenReturn("admin");
//        when(httpServletRequest.getSession()).thenReturn(session);
//
//        FilterChain filterChain = mock(FilterChain.class);
//
//        LoginFilter loginFilter = new LoginFilter();
//        loginFilter.doFilter(httpServletRequest, httpServletResponse, filterChain);
//        Mockito.verify(filterChain, Mockito.times(1))
//                .doFilter(httpServletRequest, httpServletResponse);
//    }
//
//}
