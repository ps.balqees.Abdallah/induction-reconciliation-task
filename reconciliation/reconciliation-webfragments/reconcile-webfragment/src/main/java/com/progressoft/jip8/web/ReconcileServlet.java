package com.progressoft.jip8.web;

import com.progressoft.jip8.*;
import com.progressoft.jip8.request.ReconcileRequest;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;

public class ReconcileServlet extends HttpServlet {

    // TODO this could be injected ... Done
    private Reconciliation reconciliation;
    private FunctionBasedReader<TransactionWithType> missReader;
    private FunctionBasedReader<BankTransaction> matchReader;

    public ReconcileServlet(Reconciliation reconciliation,
                            FunctionBasedReader<TransactionWithType> missReader,
                            FunctionBasedReader<BankTransaction> matchReader) {
        this.reconciliation = reconciliation;
        this.missReader = missReader;
        this.matchReader = matchReader;
    }

    @Override
    public void init()  {
        System.out.println("init was called");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        setupSession(req);
        RequestDispatcher requestDispatcher
                = req.getRequestDispatcher("/WEB-INF/views/reconcile.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try {
            HttpSession session = req.getSession();
            Path path = constructRequestThenReconcile(req);
            Path match = Paths.get(path.toString(), "match-file.csv");
            Path mismatch = Paths.get(path.toString(), "mismatch-file.csv");
            Path miss = Paths.get(path.toString(), "miss-file.csv");


            ArrayList<BankTransaction> matchList = matchReader.read(match);
            ArrayList<TransactionWithType> mismatchList = missReader.read(mismatch);
            ArrayList<TransactionWithType> missList = missReader.read(miss);

            session.setAttribute("match", matchList);
            session.setAttribute("mismatch", mismatchList);
            session.setAttribute("miss", missList);
            session.setAttribute("directory", path);

            resp.sendRedirect(req.getContextPath() + "/reconcile");

        } catch (Exception e) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
        }
    }

    private Path constructRequestThenReconcile(HttpServletRequest req) throws IOException, ParseException {

        SummaryResponse summaryResponse = (SummaryResponse) req.getSession().getAttribute("summaryResponse");
        Path resultFilesDirectory = getResultFilesDirectory(req);

        String sourceFile = File.separator + summaryResponse.getSourceFileSummary().getFilePath();
        String targetFile = File.separator + summaryResponse.getTargetFileSummary().getFilePath();

        String sourceType = summaryResponse.getSourceFileSummary().getFileType();
        String targetType = summaryResponse.getTargetFileSummary().getFileType();


        ReconcileRequest<Path> request = new WebRequest(sourceFile, targetFile
                , sourceType, targetType, resultFilesDirectory);

        return reconciliation.reconcile(request);

    }

    private Path getResultFilesDirectory(HttpServletRequest req) {
        // TODO  duplicate code from inside the doPost..Done
        Path path = Paths.get(System.getProperty("user.home") + "/resultFiles");
        File resultFiles = new File(path.toString());
        createDirectoryIfNotExist(resultFiles);

        return Paths.get(resultFiles + req.getRequestedSessionId());
    }

    private void createDirectoryIfNotExist(File file) {
        if (!file.exists()) {
            try {
                file.mkdir();
            } catch (SecurityException e) {
                throw new IllegalStateException("can't create the file", e);
            }
        }
    }

    private void setupSession(HttpServletRequest req) {
        HttpSession session = req.getSession();
        req.setAttribute("match", session.getAttribute("match"));
        req.setAttribute("mismatch", session.getAttribute("mismatch"));
        req.setAttribute("miss", session.getAttribute("miss"));
        req.setAttribute("directory", session.getAttribute("directory"));
    }

    private class WebRequest implements ReconcileRequest<Path>, Serializable {
        Path directory;
        Path sourceFile;
        Path targetFile;
        String sourceExtension;
        String targetExtension;

        public WebRequest(String sourceFile, String targetFile
                , String sourceExtension, String targetExtension, Path directory) {

            this.sourceFile = Paths.get(sourceFile);
            this.targetFile = Paths.get(targetFile);
            this.sourceExtension = sourceExtension;
            this.targetExtension = targetExtension;
            this.directory = directory;
        }

        @Override
        public Path getSourceFile() {
            return sourceFile;
        }

        @Override
        public Path getTargetFile() {
            return targetFile;
        }

        @Override
        public String getSourceExtension() {
            return sourceExtension;
        }

        @Override
        public String getTargetExtension() {
            return targetExtension;
        }

        public Path getDirectory() {
            return directory;
        }
    }


}
