<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import ="java.util.ArrayList"%>
<%@ page import ="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="jip" tagdir="/WEB-INF/tags" %>


<div class="container">
 <div class="row">
 <div class="form-group">

  <div class="tab" id="match">
           <jip:match />
           <jip:download type="match"/>
  </div>
  <div class="tab" id="mismatch">
           <jip:mismatch />
           <jip:download type="mismatch"/>
  </div>
  <div class="tab" id="miss">
           <jip:miss />
           <jip:download type="miss"/>
  </div>
  <div style="overflow:auto;">
    <div style="float:right;">
      <button type="button" id="prevBtn" onclick="nextPrev(-1)"class="btn btn-secondary">Previous</button>
      <button type="button" id="nextBtn" onclick="nextPrev(1)" class="btn btn-primary">Next</button>
      <button type="button" id="newBtn"  class="btn btn-primary" >Compare New Files</button>
    </div>
   </div>
   <div style="text-align:center;margin-top:40px;">
     <span class="step"></span>
     <span class="step"></span>
     <span class="step"></span>
   </div>
   </div>
  </div>
</div>

<script src="${pageContext.request.contextPath}/js/wizard.jsp?v=1.1"></script>
<script src="${pageContext.request.contextPath}/js/download.jsp?v=1.1"></script>

