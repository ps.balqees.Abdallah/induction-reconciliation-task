<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="jip" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<h1>Matching Table</h1>
<table class="table table-bordered">
    <thead>
      <tr>
        <th>Transaction ID</th>
        <th>Amount</th>
        <th>Currency code</th>
        <th>Date</th>
      </tr>
    </thead>
    <tbody>
    <c:forEach items="${requestScope.match}" var="item">
      <tr>
        <td>${item.reference}</td>
        <td>${item.amount}</td>
        <td>${item.currencyCode}</td>
        <td>
        <fmt:parseDate value="${active.expire_time}" pattern="yyyy-MM-dd HH:mm" var="myDate"/>
        <fmt:formatDate value="${item.date}" pattern="dd-MM-yyyy "/>
        </td>
      </tr>
      </c:forEach>
    </tbody>
  </table>
