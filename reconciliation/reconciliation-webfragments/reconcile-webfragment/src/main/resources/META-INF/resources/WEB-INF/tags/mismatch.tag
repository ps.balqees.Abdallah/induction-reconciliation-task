<%@ taglib   uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="jip" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<h1>Mismatching Table</h1>
<table class="table table-bordered">
    <thead>
      <tr>
        <th>Found in</th>
        <th>Transaction ID</th>
        <th>Amount</th>
        <th>Currency code</th>
        <th>Date</th>
      </tr>
    </thead>
    <tbody>
    <c:forEach items="${requestScope.mismatch}" var="item">
      <tr>
        <td>${item.type}</td>
        <td>${item.bankTransaction.reference}</td>
        <td>${item.bankTransaction.amount}</td>
        <td>${item.bankTransaction.currencyCode}</td>
        <td>
        <fmt:parseDate value="${active.expire_time}" pattern="yyyy-MM-dd HH:mm" var="myDate"/>
        <fmt:formatDate value="${item.bankTransaction.date}" pattern="dd-MM-yyyy "/>
        </td>
      </tr>
      </c:forEach>
    </tbody>
  </table>