<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="jip" tagdir="/WEB-INF/tags" %>
<%@attribute name="type"  required="true" %>

  <div class="row">
    <div class="col-6">
       <select id="${type}FileType" style="width:30%">
             <option value="csv">csv</option>
             <option value="json">json</option>
       </select>
    </div>
    <div class="col-6">
          <button class="btn btn-secondary downloadButton" data-type="${type}" >
                   Download
           </button>
    </div>
</div>

<br><br>