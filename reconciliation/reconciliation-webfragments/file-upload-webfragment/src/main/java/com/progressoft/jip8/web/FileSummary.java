package com.progressoft.jip8.web;


public class FileSummary {

    private String fileRef; //source or target
    private String fileName;
    private String filePath;
    private String fileType; //csv or json

    public FileSummary(String fileRef, String fileName,
                       String fileType, String filePath) {

        this.fileRef = fileRef;
        this.fileName = fileName;
        this.filePath = filePath;
        this.fileType = fileType;
    }

    public String getFileName() {
        return fileName;
    }

    public String getFileRef() {
        return fileRef;
    }

    public String getFilePath() {
        return filePath;
    }

    public String getFileType() {
        return fileType;
    }

}