package com.progressoft.jip8.web;

import org.apache.commons.io.CopyUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;

@MultipartConfig(fileSizeThreshold = 1024 * 1024,
        maxFileSize = 1024 * 1024 * 5,
        maxRequestSize = 1024 * 1024 * 5 * 5)
public class UploadServlet extends HttpServlet {
// TODO duplicate code between source and target ... Done

    private String routePath;
    private String saveDir;

    public UploadServlet(String routePath, String saveDir) {
        this.routePath = routePath;
        this.saveDir = saveDir;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] uriSplit = req.getRequestURI().split("/");
        if (uriSplit[uriSplit.length - 1].equals("targetUpload")) {
            goToView(req, resp, "/WEB-INF/views/targetUpload.jsp");
            return;
        }
        goToView(req, resp, "/WEB-INF/views/sourceUpload.jsp");
    }

    private void goToView(HttpServletRequest req, HttpServletResponse resp, String view) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher(view);
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            String fileRef = request.getParameter("fileRef");
            String fileName = request.getParameter("inputFileName");
            String fileType = request.getParameter("fileType");
            Part targetPart = request.getPart("file");
            request.getSession().setAttribute("fileRef", fileRef);
            Path path = Files.createTempFile(saveDir, fileType);

            FileSummary fileSummary =
                    new FileSummary(fileRef, fileName, fileType, path.toString());
            CopyUtils.copy(targetPart.getInputStream(), Files.newOutputStream(path));
            if (fileRef.equals("source")) {
                request.getSession().setAttribute("sourceSummery", fileSummary);
            } else {
                request.getSession().setAttribute("targetSummery", fileSummary);
            }

            response.sendRedirect(request.getContextPath() + routePath);
        } catch (Exception ex) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, ex.getMessage());
        }
    }
}