package com.progressoft.jip8.web;

import java.io.IOException;

public class FileProblemException extends RuntimeException {
    public FileProblemException(String message, IOException e) {
        super(message,e);

    }
}
