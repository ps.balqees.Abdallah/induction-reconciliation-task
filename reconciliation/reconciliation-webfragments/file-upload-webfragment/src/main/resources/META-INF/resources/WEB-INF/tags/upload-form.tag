<%@taglib prefix="jip" tagdir="/WEB-INF/tags" %>
<%@attribute name="submitUrl" rtexprvalue="true" required="true" type="java.lang.String" %>
<%@attribute name="formMethod" rtexprvalue="true" required="true" type="java.lang.String" %>
<%@attribute name="label" rtexprvalue="true" required="true" type="java.lang.String" %>
<%@attribute name="fileRef" rtexprvalue="true" required="true" type="java.lang.String" %>

<div class="container">
    <form action="${submitUrl}" enctype='multipart/form-data' method="${formMethod}" >
      <jip:input-file-name label="${label}"/>
     <p> Choose File Type : </p>
       <jip:type-list />
     <br>
     <p> Choose File to Upload in Server : </p>
        <input type="file" name="file" id="file" required />
     <br>
     <br>
     <input type="hidden" value="${fileRef}" name="fileRef"/>
     <button type="submit" class="btn btn-primary">next</button>
    </form>
</div>