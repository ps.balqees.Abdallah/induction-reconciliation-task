<%@taglib prefix="jip" tagdir="/WEB-INF/tags" %>

<jip:upload-form submitUrl="${pageContext.request.contextPath}/targetUpload"
                 label="target" fileRef = "target"
                 formMethod="POST" />

<br>

<div class="container">
   <button class="btn btn-secondary">
    <a style="text-decoration:none;color:white"
       href="${pageContext.request.contextPath}/sourceUpload">Previous</a>
    </button>
</div>