<%@attribute name="label" required="true" %>


<div class="form-group">
    <label for="${label}">${label} file name :</label>
    <input class="form-control"
           id="${label}"
           name="inputFileName"
           placeholder="Enter ${label} file name"
           onchange="getFileName(this);"
           required>
    <small class="form-text text-muted"> insert the ${label} file name.</small>
</div>

<script>
    function getFileName(myFile) {
        var filename = file.name;
    }
</script>