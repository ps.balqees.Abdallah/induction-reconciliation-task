package com.progressoft.jip8;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

public class BankTransaction {
    private String reference;
    private String currencyCode;
    private Date date;
    private BigDecimal amount;

    public BankTransaction(String reference, String currencyCode, BigDecimal amount, Date date) {
        isValidReference(reference);
        isValidCurrencyCode(currencyCode);
        isValidAmount(amount);
        isValidDate(date);
        this.reference = reference;
        this.currencyCode = currencyCode;
        this.amount = amount;
        this.date = date;
    }

    public String getReference() {
        return reference;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public Date getDate() {
        return date;
    }

    public BigDecimal getAmount() {
        return amount;
    }


    private boolean isValidAmount(BigDecimal amount) {
        if (Objects.isNull(amount))
            throw new NullPointerException("Null Amount");
        else if (amount.compareTo(BigDecimal.ZERO) <= 0)
            throw new IllegalArgumentException("Invalid Amount");
        return true;
    }

    private boolean isValidReference(String reference) {
        if (Objects.isNull(reference))
            throw new NullPointerException("Null reference");
        if (reference.contains("!") || reference.contains("*") || reference.contains("~"))
            throw new IllegalArgumentException("Invalid reference");
        return true;
    }

    private boolean isValidDate(Date date) {
        if (Objects.isNull(date))
            throw new NullPointerException("Null date");
        return true;
    }

    private boolean isValidCurrencyCode(String currencyCode) {
        if (Objects.isNull(currencyCode))
            throw new NullPointerException("Null currencyCode");
        return true;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass())
            return false;
        BankTransaction that = (BankTransaction) obj;

        return Objects.equals(reference, that.reference) &&
                Objects.equals(currencyCode, that.currencyCode) &&
                Objects.equals(date, that.date) &&
                amount.compareTo(that.amount) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(reference, currencyCode, date, amount);
    }
}
