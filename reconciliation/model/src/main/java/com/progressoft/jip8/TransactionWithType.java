package com.progressoft.jip8;

import java.util.Objects;

public class TransactionWithType {
    private String type;
    private BankTransaction bankTransaction;

    public TransactionWithType(String type, BankTransaction bankTransaction) {

        throwIfNullType(type);
        throwIfNullBankTransaction(bankTransaction);
        this.type = type;
        this.bankTransaction = bankTransaction;
    }

    public String getType() {
        return type;
    }

    public BankTransaction getBankTransaction() {
        return bankTransaction;
    }

    private void throwIfNullType(String type) {
        if (Objects.isNull(type))
            throw new NullPointerException("Null type");
    }

    private void throwIfNullBankTransaction(BankTransaction bankTransaction) {
        if (Objects.isNull(bankTransaction))
            throw new NullPointerException("Null bankTransaction");
    }


}
