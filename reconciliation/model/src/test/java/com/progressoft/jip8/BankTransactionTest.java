package com.progressoft.jip8;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Date;

public class BankTransactionTest {
    @Test
    public void givenNullReference_whenConstruct_thenThrowNullPointerException() {
        String reference = null;
        String currencyCode = "JOD";
        Date date = new Date();
        BigDecimal amount = BigDecimal.ONE;
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class
                , () -> new BankTransaction(reference, currencyCode, amount, date));
        Assertions.assertEquals("Null reference", exception.getMessage());
    }
    @Test
    public void givenNullCurrencyCode_whenConstruct_thenThrowNullPointerException() {
        String reference = "123456";
        String currencyCode =null;
        Date date = new Date();
        BigDecimal amount = BigDecimal.ONE;
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class
                , () -> new BankTransaction(reference, currencyCode, amount, date));
        Assertions.assertEquals("Null currencyCode", exception.getMessage());
    }
    @Test
    public void givenNullDate_whenConstruct_thenThrowNullPointerException() {
        String reference = "123456";
        String currencyCode ="JOD";
        Date date = null;
        BigDecimal amount = BigDecimal.ONE;
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class
                , () -> new BankTransaction(reference, currencyCode, amount, date));
        Assertions.assertEquals("Null date", exception.getMessage());
    }
    @Test
    public void givenNullAmount_whenConstruct_thenThrowNullPointerException() {
        String reference = "123456";
        String currencyCode ="JOD";
        Date date = new Date();
        BigDecimal amount = null;
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class
                , () -> new BankTransaction(reference, currencyCode, amount, date));
        Assertions.assertEquals("Null Amount", exception.getMessage());
    }
    @Test
    public void givenZeroAmount_whenConstruct_thenThrowIllegalArgumentException() {
        String reference = "123456";
        String currencyCode ="JOD";
        Date date = new Date();
        BigDecimal amount = BigDecimal.ZERO;
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class
                , () -> new BankTransaction(reference, currencyCode, amount, date));
        Assertions.assertEquals("Invalid Amount", exception.getMessage());
    }
}
