package com.progressoft.jip8;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Date;

public class TransactionWithTypeTest {
    @Test
    public void givenNullType_whenConstruct_thenThrowNullPointerException() {
        String reference = "TR-47884222201";
        String currencyCode = "JOD";
        Date date = new Date();
        BigDecimal amount = BigDecimal.ONE;
        BankTransaction bankTransaction = new BankTransaction(reference,currencyCode,amount,date);
        String type = null;
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class
                , () -> new TransactionWithType(type, bankTransaction));
        Assertions.assertEquals("Null type", exception.getMessage());
    }

    @Test
    public void givenNullBankTransaction_whenConstruct_thenThrowNullPointerException() {
        BankTransaction bankTransaction = null;
        String type = "SOURCE";
        NullPointerException exception = Assertions.assertThrows(NullPointerException.class
                , () -> new TransactionWithType(type, bankTransaction));
        Assertions.assertEquals("Null bankTransaction", exception.getMessage());
    }

}
