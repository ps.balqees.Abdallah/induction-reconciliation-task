package com.progressoft.jip8;

import com.progressoft.jip8.datasource.DataSource;
import com.progressoft.jip8.datasource.DataSourceImplementation;
import com.progressoft.jip8.request.AppRequest;
import com.progressoft.jip8.request.ReconcileRequest;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.Scanner;

public class ConsoleApp {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println(">> Enter source file location: ");
        Path sourceFile = Paths.get(scanner.next());
        System.out.println(">> Enter source file format: ");
        String sourceExtension = scanner.next();


        System.out.println(">> Enter target file location: ");
        Path targetFile = Paths.get(scanner.next());
        System.out.println(">> Enter target file format:");
        String targetExtension = scanner.next();
        Path directory = Paths.get(System.getProperty("user.home") + "/Desktop");

        ReconcileRequest appRequest = new AppRequest(sourceFile, targetFile
                , sourceExtension, targetExtension, directory);
        try {
            DataSource dataSource = new DataSourceImplementation();
            Reconciliation reconciliation = new SimpleReconciliation(dataSource);
            Path reconcilePath = reconciliation.reconcile(appRequest);

            System.out.println("Reconciliation finished.");
            System.out.println("Result files are available in directory: " + reconcilePath);
        } catch (IOException | ParseException e) {
            throw new IllegalStateException("file not found or file parsing error", e);
        }
    }
}

