message=""
clean-install:
	mvn clean install
clean:
	mvn clean
update-git:
	git add .
	git commit -m "$(message)"
	git push